#include "Voice.h"

Voice::Voice(QObject *parent) : QObject(parent)
{

}
Voice::~Voice(){}

void Voice::voiceSelect()
{
    if(_select!= nullptr)
        this->_select->play();
}

void Voice:: voiceMove()
{
    if(_move!= nullptr)
        this->_move->play();
}

void Voice:: voiceEat()
{
    if(_eat!= nullptr)
        this->_eat->play();
}



void Voice:: voiceGeneral()
{
    if(_general!= nullptr)
        this->_general->play();
}
void Voice:: voiceBackGroundMusicOn()
{
    if(_backgroundmusic!= nullptr){
        this->_backgroundmusic->play();
        _backgroundmusic->setLoops(-1);//循环次数，-1代表一直循环
    }
}
void Voice:: voiceBackGroundMusicOff(){
    if(_backgroundmusic!=nullptr){
        this->_backgroundmusic->stop();
    }
}

