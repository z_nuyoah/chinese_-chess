#ifndef STONE_H
#define STONE_H
#include <QString>
class Stone{
public:
    Stone();
    ~Stone();
    enum TYPE{CHE, MA, PAO, JIANG, SHI, BING, XIANG};

    int _row;
    int _col;
    int _id;
    bool _dead;
    bool _red;
    TYPE _type;

    void init(int id);
    QString getText(int id);

    void rotate();
};

#endif // STONE_H
