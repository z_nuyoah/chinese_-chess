#ifndef BOARD_H
#define BOARD_H

#include <QWidget>//界面控制基类
#include "Stone.h"
#include "Step.h"
#include<QMessageBox>//提示框
#include <QTimer>//定时执行一些操作用
#include <QTime>//获取当前时间
#include "Voice.h"

//声明了一个名为Ui的子命名空间，并在其中定义一个类Board。
QT_BEGIN_NAMESPACE
namespace Ui { class Board;}//Qt命名空间中的Ui子命名空间下的Board类
QT_END_NAMESPACE

class Board : public QWidget
{
    Q_OBJECT

public:
    Board(QWidget *parent = nullptr);
    ~Board();

    Stone _s[32];
    int _r;
    int _selectid;
    int _clickid;
    bool _bRedTurn;
    bool _bSide;
    bool _isOverGame;
    QVector<Step*> _steps;
    /*
     * 返回象棋棋盘行列对应的像素坐标
     */
    QPoint center(int row , int col);
    QPoint center(int id);

    bool isChecked(QPoint pt, int& row, int& col);

    void drawStone(QPainter& painter, int id);
    void init(bool bRedSide);
    void paintEvent(QPaintEvent *);
    void drawInitPosition(QPainter &p, int row, int col);
    void drawInitPosition(QPainter &);
    void mouseReleaseEvent(QMouseEvent *);
    int getStoneCountAtLine(int row1, int col1, int row2, int col2);
    int getStoneId(int row, int col);
    int movableRange(int row1, int col1, int row, int col);
    bool isBottomSide(int id);
    bool isDead(int id);

    bool isHaveAntherStep();
    void getAllPossibleMove(QVector<Step *> &steps1);
    bool canMove(int moveid, int row, int col, int killid);
    bool canMoveJIANG(int moveid, int row, int col, int killid);
    bool canMoveSHI(int moveid, int row, int col, int killid);
    bool canMoveXIANG(int moveid, int row, int col, int killid);
    bool canMoveMA(int moveid, int row, int col, int killid);
    bool canMoveCHE(int moveid, int row, int col, int killid);
    bool canMovePAO(int moveid, int row, int col, int killid);
    bool canMoveBING(int moveid, int row, int col, int killid);



    void fakeMove(Step *step);
    void unFakeMove(Step *step);
    void moveStone(int moveid, int killid, int row, int col);
    void whoWin();
    void winMessageBox(QString title, QString msg);
    virtual void back();
    void back(Step *step);
    void backOne();
    void reliveStone(int id);
    void moveStone(int moveid, int row, int col);
    void saveStep(int moveid, int killid, int row, int col, QVector<Step*>& steps);
    void killStone(int id);
    virtual void clickStone(int clickid, int row, int col);
    bool isGeneral(bool _bRedTurn);
    bool isGeneral();
    int isHongMen();
    Voice _voice;
    void click(QPoint pt);
    void seleteStone(int id);
    bool isBeSeleted(int id);
    void testMoveStone(int killid, int row, int col);
    bool isRed(int id);
    bool isSameColor(int moveid, int killid);
private slots:
    void updateTime();
    void on_pushButton_start_clicked();
    void on_pushButton_reset_clicked();
    void on_pushButton_restart_clicked();
    void on_pushButton_back_clicked();
    void on_pushButton_toMenu_clicked();

private:
    Ui::Board *ui;

    QTimer * _timer;      //定时器 每秒更新时间
    QTime * _timeRecord;  //记录时间
    bool _isStart;        //记录是否已经开始计时

signals:
    void toMenu();
};
#endif // BOARD_H
