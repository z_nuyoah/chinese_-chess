#ifndef VOICE_H
#define VOICE_H

#include <QObject>
#include <QSound>
class Voice : public QObject
{
    Q_OBJECT
public:
    explicit Voice(QObject *parent = nullptr);
    ~Voice();

    QSound *_select =new QSound(":/sound/selectChess.wav",this);
    QSound *_move =new QSound(":/sound/moveChess.wav",this);
    QSound *_eat =new QSound(":/sound/eatChess.wav",this);
    QSound *_general =new QSound(":/sound/generalSound.wav",this);
    QSound *_backgroundmusic = new QSound(":/sound/backgroundmusic.wav",this);

    void voiceSelect(); //选棋音效
    void voiceMove();   //移动音效
    void voiceEat();    //吃子音效
    void voiceGeneral();//将军音效
    void voiceBackGroundMusicOn();//背景音乐
    void voiceBackGroundMusicOff();//关闭音效


signals:

};

#endif // VOICE_H
