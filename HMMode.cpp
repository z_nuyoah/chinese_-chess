#include "HMMode.h"
#include<QMouseEvent>
#include<QTimer>
HMMode::HMMode(QWidget *parent) : Board(parent){
    _level = 3;
}

HMMode::~HMMode(){}

//选棋并移动棋子
void HMMode::clickStone(int checkedID, int row, int col){
    if(_bRedTurn){
        Board::clickStone(checkedID, row, col);
        if(!_bRedTurn){
           QTimer::singleShot(100, this, SLOT(machineMove()));
            }
        }
}
void HMMode::machineMove(){
    Step* step = getBestMove();
    moveStone(step->_moveid, step->_killid, step->_rowTo, step->_colTo);
    _selectid = -1;
    delete  step;
}
//计算最好的局面分
int HMMode::calcScore(){
    //{CHE, MA, PAO, JIANG, SHI, BING, XIANG}
    static int chessScore[]={100,50,50,20,1500,10,10};
    int redTotalScore=0;
    int blackTotalScore=0;
    //计算红棋总分
    for(int i=0;i<16;i++){
        if(_s[i]._dead)
            continue;
        redTotalScore+=chessScore[_s[i]._type];
    }
    //计算黑棋总分
    for(int i=16;i<32;i++){
        if(_s[i]._dead)
            continue;
        blackTotalScore+=chessScore[_s[i]._type];
    }
    return blackTotalScore-redTotalScore;

}
//获得最好的移动步骤
Step* HMMode::getBestMove(){
    QVector<Step *> steps;
    //获取黑棋的所有走棋路径
    Board::getAllPossibleMove(steps);
    int maxScore=-100000;
    Step* ret = NULL;
    while(steps.count()){
        Step* step= steps.last();
        steps.removeLast();
        //试着走一下
        Board::fakeMove(step);
        //获取电脑当前走法下对自己最不利的分值
        int score=getLossMinScore(_level-1, maxScore);
        //再走回来
        Board::unFakeMove(step);
        //从所有对自己最不利的分值中找到对自己最有利的分值并锁定走棋路径作为返回值返回
        if(score>maxScore){
            maxScore=score;
            if(ret) delete ret;
            ret=step;
            maxScore = score;
        }else{
            delete step;
        }
    }
    return ret;
}
int HMMode::getLossMinScore(int level, int curMin){

    if(level == 0){
        return calcScore();
    }
    QVector<Step*> steps;
    //拿到红棋的所有走棋路径
    Board::getAllPossibleMove(steps);
    int minScore=100000;
    while(steps.count()){
        Step* step= steps.last();
        steps.removeLast();
        //试着走一下红棋
        Board::fakeMove(step);
        //评估局面分
        int score=getMaxScore(level-1,minScore);
        //再走回来
        Board::unFakeMove(step);
         delete  step;
        //找到对黑棋最不利的分数作为返回值返回
        if(score <= curMin){
            while(steps.count()){
                Step* step= steps.last();
                steps.removeLast();
                delete step;
            }
            return score;
        }

        if(score<minScore){
            minScore=score;
        }

    }
    return minScore;
}
int HMMode::getMaxScore(int level, int curMax){
    if(level == 0){
        return calcScore();
    }
    QVector<Step*> steps;
    //拿到红棋的所有走棋路径
    Board::getAllPossibleMove(steps);
    int maxScore=-100000;
    while(steps.count()){
        Step* step= steps.last();
        steps.removeLast();
        //试着走一下红棋
        Board::fakeMove(step);
        //评估局面分
        int score=getLossMinScore(level-1, maxScore);
        //再走回来
        Board::unFakeMove(step);
        delete  step;
        //找到对黑棋最不利的分数作为返回值返回
        if(score >= curMax){
            while(steps.count()){
                Step* step= steps.last();
                steps.removeLast();
                delete step;
            }
            return score;
        }
        if(score > maxScore){
            maxScore=score;
        }

    }
    return maxScore;
}
