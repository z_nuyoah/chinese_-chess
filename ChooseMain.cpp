#include "ChooseMain.h"

#include <QApplication>
#include <QMessageBox>
#include <QDialog>
#include <QPushButton>
#include <QVBoxLayout>

ChooseMain::ChooseMain(QWidget *parent) : QDialog(parent){
    this->setWindowTitle("ZG_中国象棋");
    this->setFixedSize(250,120);

    QVBoxLayout* lay = new QVBoxLayout(this);
    lay->addWidget(m_buttons[0] = new QPushButton("双人模式"));
    lay->addWidget(m_buttons[1] = new QPushButton("单机模式"));


    /*游戏方式一: 两个人下棋【同一台PC机器】*/
    connect(m_buttons[0], &QPushButton::clicked,[=](){
        this->hide();
        twoPlayerMode = new Board();
        twoPlayerMode->setWindowTitle("双人模式");
        twoPlayerMode->show();

        //返回主窗口
        connect(twoPlayerMode,&Board::toMenu,[=](){
            twoPlayerMode->close();
            this->show();
        });
    });

    /*游戏方式二: 人和电脑下棋【同一台PC机器】*/
    connect(m_buttons[1], &QPushButton::clicked,[=](){
        this->hide();

        standAloneMode = new HMMode();
        standAloneMode->setWindowTitle("单机模式");
        standAloneMode->show();

        //返回主窗口
        connect(standAloneMode,&Board::toMenu,[=](){
            standAloneMode->close();
            this->show();
        });
    });

}

ChooseMain::~ChooseMain(){}
