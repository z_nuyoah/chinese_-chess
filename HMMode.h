#ifndef HMMode_H
#define HMMode_H
#include "Board.h"
#include "Step.h"
#include <QVector>//QT库中的容器，类似于STL的vector，用来存放
#include "ui_Board.h"
class HMMode : public Board{
     Q_OBJECT
public:
    explicit HMMode(QWidget *parent = 0);
    ~HMMode();

     int _level;
    int getLossMinScore(int level, int curMax);
    int getMaxScore(int level, int curMin);
    void clickStone(int checkedID, int row, int col);
    int calcScore();
    Step* getBestMove();
signals:
public slots:
    void machineMove();
};
#endif

