#ifndef CHOOSEMAIN_H
#define CHOOSEMAIN_H

#include <QWidget>
#include "Board.h"
#include "HMMode.h"


class ChooseMain : public QDialog
{
    Q_OBJECT//宏定义-->用于启动QT的元素对象系统，支持信号和槽机制，动态属性
public:
    explicit ChooseMain(QWidget *parent = nullptr);//使用explicit修饰表示该构造函数只能显式调用，而不能隐式转换。
    ~ChooseMain();

private:
    QPushButton* m_buttons[2];
    Board* twoPlayerMode;
    HMMode*standAloneMode;

};

#endif  // CHOOSEMAIN_H
